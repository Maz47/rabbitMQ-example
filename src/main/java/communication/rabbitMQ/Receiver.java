package communication.rabbitMQ;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.Delivery;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;

import communication.rabbitMQ.Utilities.LoggingCtrl;

public class Receiver {

	private String importUri;

	public Receiver(String exportUri) {
		// TODO Test 07.12.2018
		this.importUri = exportUri;
	}

	public String recvMsg(String recvQueue) {
		// TODO Test 07.12.2018
		String recvMsg = "";
		
		ConnectionFactory factory = new ConnectionFactory();
  	    Connection connection =null;
  	    Channel channel =null;
  	    try {
      		factory.setUri(importUri);
  			connection = factory.newConnection();			
  			channel = connection.createChannel();
  			channel.queueDeclare(recvQueue,true, false, false, null);
  			
  			DefaultRabbitmqRpcConsumer consumer= new DefaultRabbitmqRpcConsumer(channel);
  			channel.basicConsume(recvQueue,false ,consumer);
  			Delivery delivery = consumer.nextDelivery();
  			
  			byte[] body = delivery.getBody();
  			Envelope envelope = delivery.getEnvelope();
			recvMsg = new String(body, "UTF-8");
			long deliveryTag = envelope.getDeliveryTag();
			channel.basicAck(deliveryTag, false);
  		} catch (NullPointerException e) {
			LoggingCtrl.error(e.getMessage());
  		} catch (KeyManagementException e) {
  			LoggingCtrl.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (URISyntaxException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (IOException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (TimeoutException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (ShutdownSignalException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (ConsumerCancelledException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (InterruptedException e) {
			LoggingCtrl.error(e.getMessage());
		}finally {
  			closeChannel(channel);
  			closeConnection(connection);
  		}
		
		return recvMsg;
	}

	private void closeConnection(Connection connection) {
		try {
			if ((null!=connection) && (connection.isOpen())) connection.close();
		} catch (IOException e) {
			LoggingCtrl.error(e.getMessage());
		}
	}

	private void closeChannel(Channel channel) {
		try {
			if ((null != channel) && (channel.isOpen())) channel.close();
		} catch (IOException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (TimeoutException e) {
			LoggingCtrl.error(e.getMessage());
		}
	}
}
