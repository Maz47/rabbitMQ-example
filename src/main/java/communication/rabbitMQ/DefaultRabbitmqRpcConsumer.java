package communication.rabbitMQ;

import java.io.IOException;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.ConsumerCancelledException;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Delivery;
import com.rabbitmq.client.Envelope;
import com.rabbitmq.client.ShutdownSignalException;
import com.rabbitmq.client.RpcServer.RpcConsumer;
import com.rabbitmq.utility.Utility;

/**
 * created on 07.12.2018
 * @author mkroell
 */
public class DefaultRabbitmqRpcConsumer extends DefaultConsumer implements RpcConsumer {

    private static final Delivery POISON_DELIVERY = new Delivery(null, null, null);
    
    private final BlockingQueue<Delivery> blockingQueue;
    private volatile ShutdownSignalException shutdownSignalException = null;
    private volatile ConsumerCancelledException consumerCancelledException = null;

    public DefaultRabbitmqRpcConsumer(Channel channel) {
        this(channel, new LinkedBlockingQueue<Delivery>());
    }

    public DefaultRabbitmqRpcConsumer(Channel channel, BlockingQueue<Delivery> blockingQueue) {
        super(channel);
        this.blockingQueue = blockingQueue;
    }

    public Delivery nextDelivery() throws InterruptedException, ShutdownSignalException, ConsumerCancelledException {
        return handle(blockingQueue.take());
    }

    @Override
    public void handleShutdownSignal(String consumerTag, ShutdownSignalException signalException) {
        shutdownSignalException = signalException;
        blockingQueue.add(POISON_DELIVERY);
    }

    @Override
    public void handleCancel(String consumerTag) throws IOException {
        consumerCancelledException = new ConsumerCancelledException();
        blockingQueue.add(POISON_DELIVERY);
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body) throws IOException {
        isShutdownMode();
        this.blockingQueue.add(new Delivery(envelope, properties, body));
    }

    private Delivery handle(Delivery delivery) {
        if ((delivery == POISON_DELIVERY) || (null == delivery) && (null != shutdownSignalException || null != consumerCancelledException)) {
            if (delivery == POISON_DELIVERY) {
                blockingQueue.add(POISON_DELIVERY);
                if ((null != shutdownSignalException) && (null != consumerCancelledException)) throw new IllegalStateException("POISON in blockingQueue, but shutdownSignalException = null and consumerCancelledException = null.");
            }
            if (null != shutdownSignalException)  throw Utility.fixStackTrace(shutdownSignalException);
            if (null != consumerCancelledException) throw Utility.fixStackTrace(consumerCancelledException);
        }
        return delivery;
    }

	private void isShutdownMode() {
	    if (null != shutdownSignalException) throw Utility.fixStackTrace(shutdownSignalException);
	}
}


