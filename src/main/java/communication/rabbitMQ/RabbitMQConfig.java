package communication.rabbitMQ;

public final class RabbitMQConfig {

	public static final String TEST_QUEUE = "test_queue";
	public static final String EXPORT_URI = "export_uri";

}