package communication.rabbitMQ;

import java.util.Date;
import java.util.Vector;

import communication.rabbitMQ.Utilities.LoggingCtrl;

/**
 * created on 07.12.2018
 * 
 * @author mkroell
 */
public class RabbitMQ {

	private Vector<String> recvMsgs = new Vector<String>();
	private boolean finishedReceiving = false;

	public static void main(String[] args) {
		// TODO Test 07.12.2018
		RabbitMQ rabbit = new RabbitMQ();
		rabbit.startCommunication();
	}

	private void startCommunication() {
		// TODO Test 07.12.2018
		LoggingCtrl.info("starting Communicator ...");
		RabbitMQCommunicator communicator = new RabbitMQCommunicator();
		communicator.start();
		LoggingCtrl.info("RabbitMQCommunicator started");
		Thread receiverTask = generateReceiverTask();
		receiverTask.start();
		LoggingCtrl.info("Requestor started");
		Thread requestorTask = generateRequestorTask();
		requestorTask.start();

		LoggingCtrl.info("finish sending ...");

		long time1 = new Date().getTime();
		do {
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			LoggingCtrl.debug("waiting for messages ...");
		} while (!finishedReceiving);

		LoggingCtrl.debug("rcv-time: " + (new Date().getTime() - time1) + " ms");

		int count = 0;

		for (String recvMsg : recvMsgs) {
			LoggingCtrl.info("received MSG[" + count + "] : " + recvMsg);
			count++;
		}
	}

	private Thread generateRequestorTask() {
		// TODO Test 07.12.2018
		Thread requestorTask = new Thread(new Runnable() {

			public void run() {
				String exportUri = RabbitMQConfig.EXPORT_URI;
				String sendQueue = RabbitMQConfig.TEST_QUEUE;
				TestMessage msg = new TestMessage();
				Requestor requestor = new Requestor(exportUri);
				requestor.sendMsg(msg.getMsgID(), msg.getPayload() + " - " + new Date().getTime(), sendQueue);
				requestor.sendMsg(msg.getMsgID(), msg.getPayload() + " - " + new Date().getTime(), sendQueue);
				requestor.sendMsg(msg.getMsgID(), msg.getPayload() + " - " + new Date().getTime(), sendQueue);
				requestor.sendMsg(msg.getMsgID(), msg.getPayload() + " - " + new Date().getTime(), sendQueue);
				requestor.sendMsg(msg.getMsgID(), msg.getPayload() + " - " + new Date().getTime(), sendQueue);
				requestor.sendMsg(msg.getMsgID(), msg.getPayload() + " - " + new Date().getTime(), sendQueue);
				requestor.sendMsg(msg.getMsgID(), "finish", sendQueue);
			}
		});
		return requestorTask;
	}

	private Thread generateReceiverTask() {
		// TODO Test 07.12.2018
		Thread thread = new Thread(new Runnable() {

			public void run() {
				String exportUri = RabbitMQConfig.EXPORT_URI;
				String recvQueue = RabbitMQConfig.TEST_QUEUE;
				Receiver receiver = new Receiver(exportUri);
				LoggingCtrl.info("Receiver is waiting ...");
				String recvMsg = "";
				do {
					recvMsg = receiver.recvMsg(recvQueue);
					addRecvMsg(recvMsg);
				} while (!recvMsg.equals("finish"));
				setFinishedReceiving(true);
			}

		});
		return thread;
	}

	public void addRecvMsg(String recvMsg) {
		// TODO Test 07.12.2018
		this.recvMsgs.add(recvMsg);
	}

	private void setFinishedReceiving(boolean finishedReceiving) {
		// TODO Test 07.12.2018
		this.finishedReceiving = finishedReceiving;
	}
}
