package communication.rabbitMQ.Utilities;

public class LoggingCtrl {
	// TODO Test 07.12.2018

	private static final String INFO  = "INFO";
	private static final String DEBUG = "DEBUG";
	private static final String ERROR = "ERROR";
	
	private static final boolean INFO_TAG_ENABLED  = true;
	private static final boolean DEBUG_TAG_ENABLED = true;
	private static final boolean ERROR_TAG_ENABLED = true;

	public static void info(String msg) {
		if (INFO_TAG_ENABLED) {
			String log = getPrefix(INFO) + msg;
			log(log);
		}
	}
	
	public static void debug(String msg) {
		if (DEBUG_TAG_ENABLED) {
			String log = getPrefix(DEBUG) + msg;
			log(log);
		}
	}
	
	public static void error(String msg) {
		if (ERROR_TAG_ENABLED) {
			String log = getPrefix(ERROR) + msg;
			System.err.println(log);
		}
	}

	private static void log(String log) {
		// TODO Test 07.12.2018
		System.out.println(log);
	}

	private static String getPrefix(String tag) {
		// TODO Test 07.12.2018
		String prefix = "[" + tag + "]";
		while (prefix.length() < 8) prefix += " ";
		prefix += TimeUtilities.getIso8601Format();
		while (prefix.length() < 33) prefix += " ";
		prefix += ": ";
 		return prefix;
	}
}
