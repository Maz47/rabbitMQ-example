package communication.rabbitMQ.Utilities;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeUtilities {
	// TODO Test 07.12.2018
	
	private static final String ISO8601_TEMPLATE = "yyyy-MM-dd'T'HH:mm:ss.SSS'Z'";
	
	public static String getIso8601Format(){		
		SimpleDateFormat isoFormat = new SimpleDateFormat(ISO8601_TEMPLATE);
		isoFormat.setTimeZone(TimeZone.getTimeZone("UTC")); 
		return isoFormat.format(new Date());
	}
}
