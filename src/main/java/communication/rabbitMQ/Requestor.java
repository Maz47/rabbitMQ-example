package communication.rabbitMQ;

import java.io.IOException;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import communication.rabbitMQ.Utilities.LoggingCtrl;

public class Requestor {

	private String exportUri;

	public Requestor(String exportUri) {
		// TODO Test 07.12.2018
		this.exportUri = exportUri;
	}

	public void sendMsg(String msgID, String msgPayload, String sendQueue) {
		// TODO Test 07.12.2018
		ConnectionFactory factory = new ConnectionFactory();
		Connection connection =null;
	    Channel channel =null;
		try {
			factory.setUri(exportUri);
			connection = factory.newConnection();			
			channel = connection.createChannel();
			channel.queueDeclare(sendQueue, true, false, false, null);
			
			AMQP.BasicProperties.Builder builder = new AMQP.BasicProperties().builder();
			
			Map<String,Object> headerMap = new HashMap<String, Object>();
			headerMap.put("type", "XML");
			headerMap.put("sender", "maz");
			headerMap.put("preprocessing", "false");
			builder.headers(headerMap);
			builder.timestamp(new Date());
			builder.messageId(msgID);
			channel.basicPublish("",sendQueue,builder.build(),msgPayload.getBytes("UTF-8"));
			closeChannel(channel);
			closeConnection(connection);
		} catch (KeyManagementException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (NoSuchAlgorithmException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (URISyntaxException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (IOException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (TimeoutException e) {
			LoggingCtrl.error(e.getMessage());
		} finally {
			closeChannel(channel);
			closeConnection(connection);
		}
	}
	
	private void closeConnection(Connection connection) {
		try {
			if ((null!=connection) && (connection.isOpen())) connection.close();
		} catch (IOException e) {
			LoggingCtrl.error(e.getMessage());
		}
	}
	
	private void closeChannel(Channel channel) {
		try {
			if ((null != channel) && (channel.isOpen())) channel.close();
		} catch (IOException e) {
			LoggingCtrl.error(e.getMessage());
		} catch (TimeoutException e) {
			LoggingCtrl.error(e.getMessage());
		}
	}
}